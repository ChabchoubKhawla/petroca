package testinPackage;
	
import java.io.IOException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.esprit.entities.Forage;
import com.esprit.service.AuthenticationServiceRemote;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.fxml.FXMLLoader;


public class Main extends Application {
	@Override
	  public void start(Stage primaryStage) throws IOException {
	    Parent root = FXMLLoader.load(getClass().getResource("/GUI/PetrocaHome.fxml"));

	    Scene scene = new Scene(root);

	    primaryStage.setScene(scene);
	    primaryStage.setTitle("Petroca");
	    primaryStage.show();

	  }
	  
	
	public static void main(String[] args) throws NamingException {
		String jndi = "/Petroca-ear/Petroca-ejb/AuthenticationService!com.esprit.service.AuthenticationServiceRemote";
		Context con=new InitialContext();
		AuthenticationServiceRemote proxy=(AuthenticationServiceRemote) con.lookup(jndi);
		
		
		
		Forage list =  proxy.getAll();
		System.out.println(list.toString());
		System.out.println("Hello");
		launch(args);
	}
}
