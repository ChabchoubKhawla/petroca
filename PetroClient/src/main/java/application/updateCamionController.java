package application;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

import java.net.URL;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.esprit.entities.Camions;
import com.esprit.service.camionRemote;

import javafx.event.ActionEvent;

import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

public class updateCamionController implements Initializable {
	@FXML
	private Button btnUpdate;
	@FXML
	private ComboBox <String>state;
	@FXML
	private ComboBox<String> disponibilite;
	@FXML
	private TextField frais;
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		disponibilite.getItems().add("Available");
		disponibilite.getItems().add("Not available");
		
		state.getItems().add("Yes");
		state.getItems().add("No");
		
	}

	// Event Listener on Button[#btnUpdate].onAction
	@FXML
	public void updateCamionAction(ActionEvent event) throws NamingException {
		String jndiname="/Petroca-ear/Petroca-ejb/serviceCamion!com.esprit.service.camionRemote";
		Context con=new InitialContext();
		camionRemote proxy=(camionRemote) con.lookup(jndiname);
		
		Camions c=new Camions();
		c.setDisponibilite(disponibilite.getValue());
		c.setEtat(state.getValue());
		c.setFrais(Integer.parseInt(frais.getText()));
		
		proxy.updateCamion(c);
	}

	
}
