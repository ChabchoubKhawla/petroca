package application;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.esprit.entities.Camions;
import com.esprit.service.camionRemote;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;

import javafx.scene.input.MouseEvent;

import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;



public class readAllCamionController implements Initializable{
	@FXML
	private TableView <Camions> Tcamions;
	@FXML
	private TableColumn <Camions,String>tMatricule;
	@FXML
	private TableColumn <Camions,String >tEtat;
	@FXML
	private TableColumn <Camions,String> tDispo;
	@FXML
	private TableColumn <Camions,Integer> tcapacite;
	@FXML
	private Button btnSupprimer;
	@FXML
	private Button btnSearch;
	@FXML
	private TextField search;
	static Camions selectedCamion = new Camions();
	URL khawla000;
    ResourceBundle khawla001;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		String jndiname="/Petroca-ear/Petroca-ejb/serviceCamion!com.esprit.service.camionRemote";
		Context con;
		try {
			con = new InitialContext();
			camionRemote proxy=(camionRemote) con.lookup(jndiname);
			ArrayList<Camions> array=proxy.readAllCamion();
			ObservableList<Camions> obs=FXCollections.observableArrayList(array);
			tcapacite.setCellValueFactory(new PropertyValueFactory<>("capacite"));
			tDispo.setCellValueFactory(new PropertyValueFactory<>("disponibilite"));
			tEtat.setCellValueFactory(new PropertyValueFactory<>("etat"));
			tMatricule.setCellValueFactory(new PropertyValueFactory<>("matricule"));
			Tcamions.setItems(obs);
			
		} catch (NamingException e) {
			e.printStackTrace();
		}
		
		Tcamions.setEditable(true);
        tDispo.setCellFactory(TextFieldTableCell.forTableColumn());
        tEtat.setCellFactory(TextFieldTableCell.forTableColumn());
        tMatricule.setCellFactory(TextFieldTableCell.forTableColumn());
	}
	
	// Event Listener on TableView[#Tcamions].onMouseDragged
	@FXML
	public void selectCamion(MouseEvent event) {
		 selectedCamion= Tcamions.getSelectionModel().getSelectedItem();
	}
	// Event Listener on TableColumn[#tMatricule].onEditCommit
	@FXML
	public void changeMatriculeCellEvent(CellEditEvent<Camions, String> event) {
		 selectedCamion= Tcamions.getSelectionModel().getSelectedItem();
		selectedCamion.setMatricule(event.getNewValue().toString());
		 String jndiname="/Petroca-ear/Petroca-ejb/serviceCamion!com.esprit.service.camionRemote";
	     Context con;
		try {
			 con = new InitialContext();
			 camionRemote proxy=(camionRemote) con.lookup(jndiname);
			 proxy.updateCamion(selectedCamion);
		} catch (NamingException e) {
			e.printStackTrace();
		}
		 
	}
	// Event Listener on TableColumn[#tEtat].onEditCommit
	@FXML
	public void changeEtatCellEvent(CellEditEvent<Camions,String> event) {
		selectedCamion=Tcamions.getSelectionModel().getSelectedItem();  
		selectedCamion.setEtat(event.getNewValue().toString());
		 String jndiname="/Petroca-ear/Petroca-ejb/serviceCamion!com.esprit.service.camionRemote";
	     Context con;
		try {
			 con = new InitialContext();
			 camionRemote proxy=(camionRemote) con.lookup(jndiname);
			 proxy.updateCamion(selectedCamion);
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}
	// Event Listener on TableColumn[#tDispo].onEditCommit
	@FXML
	public void changeDispoCellEvent(CellEditEvent<Camions,String> event) {
		selectedCamion=Tcamions.getSelectionModel().getSelectedItem(); 
		selectedCamion.setDisponibilite(event.getNewValue().toString());
		 String jndiname="/Petroca-ear/Petroca-ejb/serviceCamion!com.esprit.service.camionRemote";
	     Context con;
		try {
			 con = new InitialContext();
			 camionRemote proxy=(camionRemote) con.lookup(jndiname);
			 proxy.updateCamion(selectedCamion);
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}
	// Event Listener on TableColumn[#tcapacite].onEditCommit
	@FXML
	public void changeCapaciteCellEvent(CellEditEvent<Camions,Integer> event) {
		selectedCamion=Tcamions.getSelectionModel().getSelectedItem();  
		selectedCamion.setCapacite(event.getNewValue().intValue());
		 String jndiname="/Petroca-ear/Petroca-ejb/serviceCamion!com.esprit.service.camionRemote";
	     Context con;
		try {
			 con = new InitialContext();
			 camionRemote proxy=(camionRemote) con.lookup(jndiname);
			 proxy.updateCamion(selectedCamion);
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}
	// Event Listener on Button[#btnAffecter].onAction
	@FXML
	public void SupprimerAction(ActionEvent event) {
		 String jndiname="/Petroca-ear/Petroca-ejb/serviceCamion!com.esprit.service.camionRemote";
	     Context con;
		try {
			 con = new InitialContext();
			 camionRemote proxy=(camionRemote) con.lookup(jndiname);
			 proxy.deleteCamion(Tcamions.getSelectionModel().getSelectedItem().getId());
			 ArrayList<Camions> array=proxy.readAllCamion();
				ObservableList<Camions> obs=FXCollections.observableArrayList(array);
	            Tcamions.setItems(obs);
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}
	
	// Event Listener on Button[#btnSearch].onAction
		@FXML
		public void SearchCamionAction(ActionEvent event) {
			initialize(khawla000, khawla001);
	        search.setText("");		}

		// Event Listener on TextField[#rechercheMat].onAction
				@FXML
				public void rechCamionAction(ActionEvent event) {
					 String jndiname="/Petroca-ear/Petroca-ejb/serviceCamion!com.esprit.service.camionRemote";
				     Context con;
					try {
						 con = new InitialContext();
						 camionRemote proxy=(camionRemote) con.lookup(jndiname);
						 ObservableList<Camions> filtred=FXCollections.observableArrayList();  

						 ArrayList<Camions> array=proxy.searchCamion(search.getText());
						 	filtred=FXCollections.observableArrayList(array);
							tcapacite.setCellValueFactory(new PropertyValueFactory<>("capacite"));
							tDispo.setCellValueFactory(new PropertyValueFactory<>("disponibilite"));
							tEtat.setCellValueFactory(new PropertyValueFactory<>("etat"));
							tMatricule.setCellValueFactory(new PropertyValueFactory<>("matricule"));
							Tcamions.setItems(filtred);
					} catch (NamingException e) {
						e.printStackTrace();
					}
				}
}






