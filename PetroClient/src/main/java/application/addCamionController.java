package application;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;


import com.esprit.entities.Camions;
import com.esprit.service.camionRemote;

import javafx.event.ActionEvent;

public class addCamionController implements Initializable {
	@FXML
	private TextField matricule;
	@FXML
	private TextField capacite;
	@FXML
	private Button addCamion;
	@FXML
	private TextField marque;
	@FXML
	private TextField prix;

	// Event Listener on Button[#addCamion].onAction
	@FXML
	public void addCamionAction(ActionEvent event) throws NamingException {
		String jndiname="/Petroca-ear/Petroca-ejb/serviceCamion!com.esprit.service.camionRemote";
		Context con=new InitialContext();
		camionRemote proxy=(camionRemote) con.lookup(jndiname);
		
		
		Camions c=new Camions();
		
		c.setMarque(marque.getText());
		c.setMatricule(matricule.getText());

		
		
		c.setPrix(Integer.parseInt(prix.getText()));
		c.setCapacite(Integer.parseInt(capacite.getText()));
		
		proxy.addCamion(c);
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
	}
}



