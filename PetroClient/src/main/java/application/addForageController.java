package application;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.esprit.entities.Extraction;
import com.esprit.entities.Forage;
import com.esprit.service.extractionRemote;
import com.esprit.service.forageRemote;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;

import javafx.scene.control.ComboBox;

public class addForageController implements Initializable{
	@FXML
	private Button btnAdd;
	@FXML
	private ComboBox<String>  cbExtraction;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		String jndiname3="/Petroca-ear/Petroca-ejb/serviceExtraction!com.esprit.service.extractionRemote";
		Context con3;
		try {
			con3 = new InitialContext();
			extractionRemote proxy3=(extractionRemote) con3.lookup(jndiname3);
			ArrayList<Extraction> array=proxy3.readAllExtraction();
			ObservableList<Extraction> obs=FXCollections.observableArrayList(array);
			 for (int i = 0; i < obs.size(); i++) {
		            Extraction e = obs.get(i);
		            cbExtraction.getItems().add(e.getAddresse());
		            cbExtraction.setValue(e.getAddresse());
		        }
		
		} catch (NamingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
			
	}
	
	// Event Listener on Button[#btnAdd].onAction
	@FXML
	public void addForageAction(ActionEvent event) throws NamingException {
		Extraction e=new Extraction();
		String jndiname1="/Petroca-ear/Petroca-ejb/serviceForage!com.esprit.service.forageRemote";
		Context con1=new InitialContext();
		forageRemote proxy1=(forageRemote) con1.lookup(jndiname1);
		
		String jndiname3="/Petroca-ear/Petroca-ejb/serviceExtraction!com.esprit.service.extractionRemote";
		Context con3=new InitialContext();
		extractionRemote proxy3=(extractionRemote) con3.lookup(jndiname3);
		ArrayList<Extraction> array=proxy3.readAllExtraction();
		ObservableList<Extraction> obs=FXCollections.observableArrayList(array);
		Forage f=new Forage();
        for (int i = 0; i < obs.size(); i++) {
             e = obs.get(i);
            if (cbExtraction.getValue().equals(e.getAddresse())) {
                f.setEtat("Non Valide");
                f.setExtraction(e);
                proxy1.addForage(f);
            }
        }
        
        
	}

	
}
