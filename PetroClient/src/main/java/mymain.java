
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.esprit.entities.Camions;
import com.esprit.entities.Extraction;
import com.esprit.entities.Forage;
import com.esprit.entities.Puit;
import com.esprit.entities.Users;



public class mymain {

	public static void main(String[] args) throws NamingException, ParseException, SQLException, ClassNotFoundException, NoClassDefFoundError, RuntimeException {

		/*Camions
		String jndiname="/Petroca-ear/Petroca-ejb/serviceCamion!com.esprit.service.camionRemote";
		Context con=new InitialContext();
		camionRemote proxy=(camionRemote) con.lookup(jndiname);
		
		
		Camions c1=new Camions();
		c1.setCapacite(154);
		c1.setDisponibilite("Disponible");
		c1.setEtat("Bon");
		c1.setId(1);
		c1.setMatricule("8475tunis185");
		//proxy.addCamion(c1);
		ArrayList<Camions> arr=proxy.readAllCamion();
		if(arr.size()!=0){
		for(int i=0;i<arr.size();i++){
			
			Camions C=(Camions) arr.get(i);
			System.out.println(C.toString());
		}
		}else{
			System.out.println("Il n'y a aucun camion");
		}
		
		ArrayList<Camions> a=proxy.searchCamion("847k5tunis185");
		if(a.size()!=0){
			for(int i=0;i<a.size();i++){
				
				Camions C=(Camions) a.get(i);
				System.out.println(C.toString());
			}
			}else{
				System.out.println("la matricule n'existe pas");
			}
			
		Camions cam=proxy.sreachCamionId(2);
		if(cam!=null){
		System.out.println(cam);
		}else{
			System.out.println("le camion n'existe pas");
		}
		
		c1.setMatricule("SaifAllela");
		
				
		//proxy.updateCamion(c1);
		
		//proxy.deleteCamion(2);
		
		//proxy.AffecterMissionCamion(1, c1.getId());
		//proxy.AffecterUserCamion(2, c1.getId());
		
		//System.out.println(c1.toString());
	//Forage
		String jndiname1="/Petroca-ear/Petroca-ejb/serviceForage!com.esprit.service.forageRemote";
		Context con1=new InitialContext();
		forageRemote proxy1=(forageRemote) con1.lookup(jndiname1);
			
		Forage f1=new Forage();
		Forage f2=new Forage();
		Forage f3=new Forage();
		f1.setEtat("EnAttente");
		f1.setId(1);
		f2.setEtat("EnAttente");
		f2.setId(2);
		f3.setEtat("Valide");
		f3.setId(3);
		/*proxy1.addForage(f1);
		proxy1.addForage(f3);
		proxy1.addForage(f2);
		ArrayList<Forage> array=proxy1.readAllForage();
		if(array.size()!=0){
		for(int i=0;i<array.size();i++){
			Forage f=(Forage) array.get(i);
			System.out.println(f.toString());
			}
		}else{
			System.out.println("Liste de forage vide");
		}
		
		ArrayList<Forage> F=proxy1.searchForage("EnAttente");
		if(F.size()!=0){
			for(int i=0;i<F.size();i++){
				Forage f=(Forage) F.get(i);
				System.out.println(f.toString());
				}
			}else{
				System.out.println("Ce forage n'existe pas");
			}
		
		f2.setEtat("Valide");
		//proxy1.updateForage(f2);
		
		Forage f=proxy1.searchId(0);
		if(f!=null){
			System.out.println(f.toString());
		}else{
			System.out.println("ce forage n'existe paas");
		}
		//proxy1.removeForage(0);
		
		
		//Puit
		String jndiname2="/Petroca-ear/Petroca-ejb/servicePuit!com.esprit.service.puitRemote";
		Context con2=new InitialContext();
		puitRemote proxy2=(puitRemote) con2.lookup(jndiname2);
		
		Puit p1=new Puit();
		
		p1.setCout(458);
		p1.setDate_production(new Date());
		p1.setEtat("EnCours");
		p1.setId(2);
		p1.setQuantite(152);
		
		//proxy2.addPuit(p1);
		
		Puit p2=new Puit();
				p2.setCout(999);
				p2.setDate_production(new Date());
				p2.setEtat("EnCours");
				p2.setId(555);
				p2.setQuantite(741);
		ArrayList<Forage> array1=proxy1.readAllForage();
		if(array1.size()!=0){
			for(int i=0;i<array1.size();i++){
				Forage forage=(Forage) array1.get(i);
				if(forage.getEtat().equals("Valide")){
					proxy1.passerForageAPuit(forage.getId(), p2);
					}
				}
			}else{
			System.out.println("liste vide");
		}
		
		p1.setCout(123);
		//p2.setExtraction(e);
		//proxy2.updatePuit(p1);
		
		ArrayList<Puit> results=proxy2.readAllPuit();
		if(results.size()!=0){
		for(int i=0;i<results.size();i++){
			Puit p=(Puit)results.get(i);
			System.out.println(p.toString());
		}
		}else{
			System.out.println("pas de puits");
		}

		Puit pp=proxy2.searchById(2);
		if(pp!=null){
			System.out.println(pp.toString());
		}else{
			System.out.println("n'existe pas");
		}
		
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
	
			Date d1=sdf.parse("2019-03-27");
			Date d2=sdf.parse("2019-03-26");
			Date d3=sdf.parse("2019-03-28");
		
		ArrayList<Puit> res=proxy2.searchPuitByDate(new Date());
		if(res.size()!=0){
			for(int i=0;i<res.size();i++){
				Puit p=(Puit)res.get(i);
				System.out.println(p.toString());
			}
			}else{
				System.out.println("pas de puitssss");
			}
		
		
		//proxy2.AffecterExtractionAPuit(1, p1.getId());

		ArrayList <Puit> list=proxy2.searchByState("EnCou rs");
		if(list.size()!=0){
			for(int i=0;i<list.size();i++){
				Puit p=(Puit)list.get(i);
				System.out.println(p.toString());
			}
			}else{
				System.out.println("famech");
			}
		Puit par=proxy2.searchById(2);
		if(par!=null){
			System.out.println(par.toString());
		}else{
			System.out.println("fam chay");
		}
 
		
		
		//Extraction
		String jndiname3="/Petroca-ear/Petroca-ejb/serviceExtraction!com.esprit.service.extractionRemote";
		Context con3=new InitialContext();
		extractionRemote proxy3=(extractionRemote) con3.lookup(jndiname3);
		
		Puit p=new Puit();
		p.setCout(123);
		p.setDate_production(d1);
		p.setEtat("Valide");
		p.setId(5);
		p.setQuantite(456);
		
		Users user=new Users();
		user.setNom("SiAllela");
		user.setPt_fidelite(77);
		user.setRole(Role.PDG);
		
		Extraction e=new Extraction();
		e.setAddresse("LaGhazela");
		e.setDate_debut(d2);
		e.setEtat("EnCours");
		e.setFin_estimation(d3);
		//e.addPuit(p);
		e.addUser(user);
		
		//proxy3.addExtraction(e);
		//proxy3.deleteExtraction(13);
		
		ArrayList<Extraction> array2=proxy3.readAllExtraction();
		if(array2.size()!=0){
			for(int i=0;i<array2.size();i++){
			Extraction ex=(Extraction) array2.get(i);
			System.out.println(ex.toString());
			}
		}else{
			System.out.println("fam chayyyyy");
		}
		
		e.setAddresse("mazel");
		proxy3.updateExtraction(e);
		System.out.println(proxy3.updateExtraction(e));
		
		ArrayList<Extraction> array3=proxy3.searchByEtat("EnAttente");
		if(array3.size()!=0){
			for(int i=0;i<array3.size();i++){
			Extraction ex=(Extraction) array3.get(i);
			System.out.println(ex.toString());
			}
		}else{
			System.out.println("fam hatchayy");
		}
	*/	
	}	
}
