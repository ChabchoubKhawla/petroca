/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.esprit.entities.Forage;
import com.esprit.entities.Users;
import com.esprit.service.AuthenticationServiceRemote;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

/**
 * FXML Controller class
 *
 * @author omarfarouklakhdhar
 */
public class PetrocaHomeController implements Initializable {

  @FXML
  private TextField email;
  @FXML
  private TextField password;
  @FXML
  private Button loginButton;
  @FXML
  private Text error;

  /**
   * Initializes the controller class.
   */
  @Override
  public void initialize(URL url, ResourceBundle rb) {
	 
  }  

  @FXML
  public void authenticateUser(ActionEvent event) throws  IOException, NamingException {
	  	
	  	Users user;
	  	
	  	FXMLLoader loader = new FXMLLoader(getClass().getResource("/GUI/PetrocaDashboard.fxml"));
        Parent root = (Parent) loader.load(); 
        
        String jndi = "/Petroca-ear/Petroca-ejb/AuthenticationService!com.esprit.service.AuthenticationServiceRemote";
		Context con=new InitialContext();
		AuthenticationServiceRemote proxy=(AuthenticationServiceRemote) con.lookup(jndi);
		
		user = proxy.authenticate(email.getText(), password.getText());
		System.out.println(user.toString());
		Forage list =  proxy.getAll();
		System.out.println(user.toString());
		System.out.println("from controller");
		if(!user.equals(null)){
			PetrocaDashboardController controller = loader.<PetrocaDashboardController>getController();
			controller.initData(user);
			loginButton.getScene().setRoot(root);
		} else {
			error.setText("Please check your email and password");
		}
        
  }
  
}
