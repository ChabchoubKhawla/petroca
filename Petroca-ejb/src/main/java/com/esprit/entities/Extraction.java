package com.esprit.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Extraction
 *
 */
@Entity

public class Extraction implements Serializable {

	   
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String addresse;
	private String etat;
	private Date date_debut;
	private Date fin_estimation;
	@OneToMany(mappedBy="extraction")
	private List<Users> users;
	@OneToMany(mappedBy="extraction")
	private List<Puit> puit;
	private static final long serialVersionUID = 1L;

	public Extraction() {
		super();
	}   
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAddresse() {
		return addresse;
	}
	public void setAddresse(String addresse) {
		this.addresse = addresse;
	}
	public String getEtat() {
		return etat;
	}
	public void setEtat(String etat) {
		this.etat = etat;
	}
	public Date getDate_debut() {
		return date_debut;
	}
	public void setDate_debut(Date date_debut) {
		this.date_debut = date_debut;
	}
	public Date getFin_estimation() {
		return fin_estimation;
	}
	public void setFin_estimation(Date fin_estimation) {
		this.fin_estimation = fin_estimation;
	}
	public List<Users> getUsers() {
		return users;
	}
	public void setUsers(List<Users> users) {
		this.users = users;
	}
	public List<Puit> getPuit() {
		return puit;
	}
	public void setPuit(List<Puit> puit) {
		this.puit = puit;
	}
	
   
}
