package com.esprit.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Pompe
 *
 */
@Entity

public class Pompe implements Serializable {

	   
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@OneToMany(mappedBy="pompe")
	private List<Users> users;
	@OneToOne
	private Volucompteur volucompteur;
	private static final long serialVersionUID = 1L;
	@ManyToOne
	private Station station;
	@OneToMany(mappedBy="pompe")
	private List<Produit> produit;
	
	public List<Produit> getProduit() {
		return produit;
	}
	public void setProduit(List<Produit> produit) {
		this.produit = produit;
	}
	public Station getStation() {
		return station;
	}
	public void setStation(Station station) {
		this.station = station;
	}
	public Pompe() {
		super();
	}   
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public List<Users> getUsers() {
		return users;
	}
	public void setUsers(List<Users> users) {
		this.users = users;
	}
	
	public Volucompteur getVolucompteur() {
		return volucompteur;
	}
	public void setVolucompteur(Volucompteur volucompteur) {
		this.volucompteur = volucompteur;
	}
   
}
