package com.esprit.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Stock
 *
 */


public class Stock implements Serializable {

	
	private int id;
	private String name;
	private int quantity;
	private float price;
	
	private static final long serialVersionUID = 1L;

	public Stock() {
		super();
	}   
	
	public Stock(int id,String name, int quantity, float price) {
		this.id= id;
		this.name = name;
		this.quantity = quantity;
		this.price = price;
	}
	
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}   
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}   
	public int getQuantity() {
		return this.quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}   
	public float getPrice() {
		return this.price;
	}

	public void setPrice(float price) {
		this.price = price;
	}
   
}
