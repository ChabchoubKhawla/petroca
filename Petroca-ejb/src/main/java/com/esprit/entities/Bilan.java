package com.esprit.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Bilan
 *
 */
@Entity

public class Bilan implements Serializable {

	   
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private double passive;
	private double active;
	private Date date;
	@OneToMany(mappedBy="bilan")
	private List<Transaction> transaction;
	@ManyToOne
	private Users user;
	private static final long serialVersionUID = 1L;

	public void setTransaction(List<Transaction> transaction) {
		this.transaction = transaction;
	}
	public Users getUser() {
		return user;
	}
	public void setUser(Users user) {
		this.user = user;
	}
	public Bilan() {
		super();
	}   
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public List<Transaction> getTransaction() {
		return transaction;
	}
	public double getPassive() {
		return passive;
	}
	public void setPassive(double passive) {
		this.passive = passive;
	}
	public double getActive() {
		return active;
	}
	public void setActive(double active) {
		this.active = active;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
   
}
