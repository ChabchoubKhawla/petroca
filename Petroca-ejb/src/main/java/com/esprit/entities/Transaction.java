package com.esprit.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import com.esprit.type.TypeTransaction;

/**
 * Entity implementation class for Entity: Transaction
 *
 */
@Entity

public class Transaction implements Serializable {

	   
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String donnee;
	private Date date_transaction;
    private double montant;
    
    private TypeTransaction type;
	private static final long serialVersionUID = 1L;
	@ManyToOne
	private Bilan bilan;
	
	@ManyToOne
	@JoinColumn(name="id_produit",referencedColumnName="id",insertable=false,updatable=false)
	private Produit produit;
	
	@ManyToOne
	@JoinColumn(name="id_user",referencedColumnName="id",insertable=false,updatable=false)
	private Users user;

	public Bilan getBilan() {
		return bilan;
	}
	public void setBilan(Bilan bilan) {
		this.bilan = bilan;
	}
	public Produit getProduit() {
		return produit;
	}
	public void setProduit(Produit produit) {
		this.produit = produit;
	}
	public Users getUser() {
		return user;
	}
	public void setUser(Users user) {
		this.user = user;
	}
	public Transaction() {
		super();
	}   
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDonnee() {
		return donnee;
	}
	public void setDonnee(String donnee) {
		this.donnee = donnee;
	}
	public Date getDate_transaction() {
		return date_transaction;
	}
	public void setDate_transaction(Date date_transaction) {
		this.date_transaction = date_transaction;
	}
	public double getMontant() {
		return montant;
	}
	public void setMontant(double montant) {
		this.montant = montant;
	}
   
}
