package com.esprit.entities;

import java.io.Serializable;
import java.lang.String;
import java.sql.Date;
import java.util.List;

import javax.persistence.*;

import com.esprit.type.*;



/**
 * Entity implementation class for Entity: Users
 *
 */
@Entity

public class Users implements Serializable {
	
	   
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String name;
	private String surname;
	private String email;
	private Date birthday;
	private int isActive; /// 0 for working 1 for terminated
	private String password;
	
	@Enumerated(EnumType.STRING)
	private TypeContract contractType;
	private float Salary;
	private String username;
	
	
	@Enumerated(EnumType.STRING)
	private TypeStatusRec recStatus;
	//@OneToMany(mappedBy="user")
	//private List<Transaction> transaction;
	@OneToMany(mappedBy="users",fetch = FetchType.EAGER)
	private List<Recrutement> recrutement;
	private int pt_fidelite;
	@ManyToOne
	private Pompe pompe;
	@OneToMany(mappedBy="users",fetch = FetchType.EAGER)
	private List<Reclamation> reclamation;
	@ManyToOne
	private Extraction extraction;
	@OneToMany(mappedBy="user",fetch = FetchType.EAGER)
	private List<Bilan> bilan;
	@OneToMany(mappedBy="user",fetch = FetchType.EAGER)
	private List<Roulant> roulant;
	@ManyToOne
	private Mission mission;
	
	@ManyToOne
	private Front_Section front_section;
	
	@OneToMany(mappedBy="user",fetch = FetchType.EAGER)
	private List<Lavage> lavage;
	
	
	
	@ManyToOne
	private Station station;
	
	@Enumerated(EnumType.STRING)
	private TypeUser userType;
	
	private static final long serialVersionUID = 1L;
	
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	public Front_Section getFront_section() {
		return front_section;
	}
	public void setFront_section(Front_Section front_section) {
		this.front_section = front_section;
	}
	public Station getStation() {
		return station;
	}
	public void setStation(Station station) {
		this.station = station;
	}
	
	public List<Lavage> getLavage() {
		return lavage;
	}
	public void setLavage(List<Lavage> lavage) {
		this.lavage = lavage;
	}
	public Mission getMission() {
		return mission;
	}
	public void setMission(Mission mission) {
		this.mission = mission;
	}
	public List<Roulant> getRoulant() {
		return roulant;
	}
	public void setRoulant(List<Roulant> roulant) {
		this.roulant = roulant;
	}
	public List<Bilan> getBilan() {
		return bilan;
	}
	public void setBilan(List<Bilan> bilan) {
		this.bilan = bilan;
	}

	public List<Recrutement> getRecrutement() {
		return recrutement;
	}
	public void setRecrutement(List<Recrutement> recrutement) {
		this.recrutement = recrutement;
	}
	public Pompe getPompe() {
		return pompe;
	}
	public void setPompe(Pompe pompe) {
		this.pompe = pompe;
	}

	

	public Users() {
		super();
	}   
	   
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	  
	  
	public int getPt_fidelite() {
		return this.pt_fidelite;
	}

	public void setPt_fidelite(int pt_fidelite) {
		this.pt_fidelite = pt_fidelite;
	}
	public List<Reclamation> getReclamation() {
		return reclamation;
	}
	public void setReclamation(List<Reclamation> reclamation) {
		this.reclamation = reclamation;
	}

	public Extraction getExtraction() {
		return extraction;
	}
	public void setExtraction(Extraction extraction) {
		this.extraction = extraction;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public int getIsActive() {
		return isActive;
	}
	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public TypeContract getContractType() {
		return contractType;
	}
	public void setContractType(TypeContract contractType) {
		this.contractType = contractType;
	}
	public float getSalary() {
		return Salary;
	}
	public void setSalary(float salary) {
		Salary = salary;
	}
	public TypeStatusRec getRecStatus() {
		return recStatus;
	}
	public void setRecStatus(TypeStatusRec recStatus) {
		this.recStatus = recStatus;
	}
	public TypeUser getUserType() {
		return userType;
	}
	public void setUserType(TypeUser userType) {
		this.userType = userType;
	}
	@Override
	public String toString() {
		return "Users [id=" + id + ", name=" + name + ", surname=" + surname + ", email=" + email + ", birthday="
				+ birthday + ", isActive=" + isActive + ", password=" + password + ", contractType=" + contractType
				+ ", Salary=" + Salary + ", username=" + username + ", recStatus=" + recStatus + ", recrutement="
				+   ", pt_fidelite=" + pt_fidelite + ", pompe=" + pompe + ", reclamation=" + reclamation
				+ ", extraction=" + extraction + ", bilan=" + bilan + ", roulant="  +", mission=" + mission
				+ ", front_section=" + front_section + ", lavage=" + lavage + ", user="  + ", station=" + station
				+ ", userType=" + userType + "]";
	}
	
   
}
