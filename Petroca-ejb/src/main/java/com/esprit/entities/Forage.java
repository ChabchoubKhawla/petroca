package com.esprit.entities;

import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Forage
 *
 */
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Forage implements Serializable {

	   
	@Id
	private int id;
	private String etat;

	private static final long serialVersionUID = 1L;

	public Forage() {
		super();
	}   
	  
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEtat() {
		return this.etat;
	}

	public void setEtat(String etat) {
		this.etat = etat;
	}

	@Override
	public String toString() {
		return "Forage [id=" + id + ", etat=" + etat + "]";
	}

	
}
