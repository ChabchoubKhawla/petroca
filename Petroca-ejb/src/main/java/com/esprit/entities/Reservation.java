package com.esprit.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Reservation
 *
 */
@Entity

public class Reservation implements Serializable {

	   
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private Date date_reservation;
	@ManyToOne
	//@JoinColumn(name="id_user",referencedColumnName="id_user",insertable=false,updatable=false)
	private Users users;
	
	public Users getUsers() {
		return users;
	}
	public void setUsers(Users users) {
		this.users = users;
	}
	@ManyToOne
	//@JoinColumn(name="id_lavage",referencedColumnName="id_lavage",insertable=false,updatable=false)
	private Lavage lavage;
	
	
	public Lavage getLavage() {
		return lavage;
	}
	public void setLavage(Lavage lavage) {
		this.lavage = lavage;
	}
	private static final long serialVersionUID = 1L;

	public Reservation() {
		super();
	}   
	  
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getDate_reservation() {
		return this.date_reservation;
	}

	public void setDate_reservation(Date date_reservation) {
		this.date_reservation = date_reservation;
	}
   
}
