package com.esprit.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import com.esprit.type.TypeReclamation;

/**
 * Entity implementation class for Entity: Reclamation
 *
 */
@Entity
@Table(name="reclamation")
public class Reclamation implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	//@Temporal(TemporalType.TIMESTAMP)
	private Date date;
	private String description;
	@Enumerated(EnumType.STRING)
	private EtatReclamation etat = EtatReclamation.En_attente;
	@Enumerated(EnumType.STRING)
	private TypeReclamation type;
	@ManyToOne
	private Users users;
	
	private static final long serialVersionUID = 1L;

	public Reclamation() {
		super();
	}
	
	
	public Reclamation(Date date, String description, TypeReclamation type) {
		super();
		this.date = date;
		this.description = description;
		this.type = type;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public Date getDate() {
		return date;
	}


	public void setDate(Date date) {
		this.date = date;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public TypeReclamation getType() {
		return type;
	}


	public void setType(TypeReclamation type) {
		this.type = type;
	}


	public EtatReclamation getEtat() {
		return etat;
	}


	public void setEtat(EtatReclamation etat) {
		this.etat = etat;
	}


	public Users getUsers() {
		return users;
	}


	public void setUsers(Users users) {
		this.users = users;
	}


	@Override
	public String toString() {
		return "Reclamation [id=" + id + ", date=" + date + ", description=" + description + ", etat=" + etat
				+ ", type=" + type + ", users=" + users.getName() + "]";
	}


	

}
