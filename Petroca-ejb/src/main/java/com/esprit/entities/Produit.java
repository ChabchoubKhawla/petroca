package com.esprit.entities;


import java.io.Serializable;

import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Produit
 *
 */
@Entity

public class Produit implements Serializable {
	public enum Type_produit {
		 Unleaded_gasoline_95,Unleaded_gasoline_98,Natural_Gaz_For_Vehicules,
		 GASOIL_50,GASOIL;  
		}
	   
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private Type_produit type;
	
	private float quantite;
	@OneToMany(mappedBy="produit")
	private List<Transaction> transaction;
	@ManyToOne
    private Pompe pompe;
	
	@ManyToOne
	private Station station;
	
	
	public Type_produit getType() {
		return type;
	}
	public void setType(Type_produit type) {
		this.type = type;
	}
	
	public Pompe getPompe() {
		return pompe;
	}
	public void setPompe(Pompe pompe) {
		this.pompe = pompe;
	}
	public Station getStation() {
		return station;
	}
	public void setStation(Station station) {
		this.station = station;
	}
	public List<Transaction> getTransaction() {
		return transaction;
	}
	public void setTransaction(List<Transaction> transaction) {
		this.transaction = transaction; 
	}
	
	private static final long serialVersionUID = 1L;

	public Produit() {
		super();
	}   
	  
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public float getQuantite() {
		return quantite;
	}
	public void setQuantite(float quantite) {
		this.quantite = quantite;
	}
   
}
