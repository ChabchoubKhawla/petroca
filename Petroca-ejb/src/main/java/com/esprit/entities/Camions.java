package com.esprit.entities;

import java.io.Serializable;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Camions
 *
 */
@Entity
public class Camions extends Roulant implements Serializable {

	   


	private float capacite;
	private static final long serialVersionUID = 1L;
	@ManyToOne
	private Mission mission;
	
	public Mission getMission() {
		return mission;
	}
	public void setMission(Mission mission) {
		this.mission = mission;
	}
	public Camions() {
		super();
	}   
  
	  
	public float getCapacite() {
		return this.capacite;
	}

	public void setCapacite(float capacite) {
		this.capacite = capacite;
	}
   
}
