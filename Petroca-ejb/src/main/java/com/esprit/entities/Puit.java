package com.esprit.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Puit
 *
 */
@Entity

public class Puit extends Forage implements Serializable {

	   

	private double cout;
	private Date date_production;
	private double quantite;
	@ManyToOne
	private Extraction extraction;
	
	private static final long serialVersionUID = 1L;


	public Puit() {
		super();
	}   
	
	public double getCout() {
		return cout;
	}
	public void setCout(double cout) {
		this.cout = cout;
	}
	public Date getDate_production() {
		return date_production;
	}
	public void setDate_production(Date date_production) {
		this.date_production = date_production;
	}
	public double getQuantite() {
		return quantite;
	}
	public void setQuantite(double quantite) {
		this.quantite = quantite;
	}
	public Extraction getExtraction() {
		return extraction;
	}
	public void setExtraction(Extraction extraction) {
		this.extraction = extraction;
	}

   
}
