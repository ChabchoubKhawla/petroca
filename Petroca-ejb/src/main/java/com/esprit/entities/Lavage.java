package com.esprit.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Lavage
 *
 */
@Entity

public class Lavage implements Serializable {

	   
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private int nbre_workers;
	private static final long serialVersionUID = 1L;
	@ManyToOne
	private Station station;
	@OneToMany(mappedBy="lavage")
	private List<Reservation> reservation;
	
	@ManyToOne
	private Users user;
	
	
	public Station getStation() {
		return station;
	}
	public void setStation(Station station) {
		this.station = station;
	}
	public Users getUser() {
		return user;
	}
	public void setUser(Users user) {
		this.user = user;
	}
	public List<Reservation> getReservation() {
		return reservation;
	}
	public void setReservation(List<Reservation> reservation) {
		this.reservation = reservation;
	}
	public Lavage() {
		super();
	}   
	  
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getNbre_workers() {
		return this.nbre_workers;
	}

	public void setNbre_workers(int nbre_workers) {
		this.nbre_workers = nbre_workers;
	}
   
}
