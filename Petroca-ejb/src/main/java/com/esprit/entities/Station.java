package com.esprit.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Station
 *
 */
@Entity

public class Station implements Serializable {

	   
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	
	
	private float budget;
	
	
    
    @OneToMany(mappedBy="station")
    private List<Lavage> lavage;
    @OneToMany(mappedBy="station")
    private List<Pompe> pompe;
    @OneToMany(mappedBy="station")
    private List<Volucompteur> volucompteur;
    @OneToMany(mappedBy="station")
    private List<Produit> produit;
    @OneToMany(mappedBy="station")
    private List<Users> user;
    
    
	private static final long serialVersionUID = 1L;

	public List<Users> getUser() {
		return user;
	}
	public void setUser(List<Users> user) {
		this.user = user;
	}
	public Station() {
		super();
	}   
	  
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public float getBudget() {
		return this.budget;
	}

	public void setBudget(float budget) {
		this.budget = budget;
	}
	
	public List<Volucompteur> getVolucompteur() {
		return volucompteur;
	}
	public void setVolucompteur(List<Volucompteur> volucompteur) {
		this.volucompteur = volucompteur;
	}
	public List<Produit> getProduit() {
		return produit;
	}
	public void setProduit(List<Produit> produit) {
		this.produit = produit;
	}
	public List<Lavage> getLavage() {
		return lavage;
	}
	public void setLavage(List<Lavage> lavage) {
		this.lavage = lavage;
	}
	public List<Pompe> getPompe() {
		return pompe;
	}
	public void setPompe(List<Pompe> pompe) {
		this.pompe = pompe;
	}
   
}
