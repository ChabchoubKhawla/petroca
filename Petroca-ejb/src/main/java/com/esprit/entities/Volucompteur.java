package com.esprit.entities;

import java.io.Serializable;


import javax.persistence.*;

/**
 * Entity implementation class for Entity: Volucompteur
 *
 */
@Entity

public class Volucompteur implements Serializable {

	   
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private float capacite;
	private float quantite;
	@OneToOne(mappedBy= "volucompteur")
	private Pompe pompe;
	@ManyToOne
	private Station station;
	
	private static final long serialVersionUID = 1L;

	public Station getStation() {
		return station;
	}
	public void setStation(Station station) {
		this.station = station;
	}
	public Volucompteur() {
		super();
	}   
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public float getCapacite() {
		return capacite;
	}
	public void setCapacite(float capacite) {
		this.capacite = capacite;
	}
	public float getQuantite() {
		return quantite;
	}
	public void setQuantite(float quantite) {
		this.quantite = quantite;
	}
	
	public Pompe getPompe() {
		return pompe;
	}
	public void setPompe(Pompe pompe) {
		this.pompe = pompe;
	}
   
}
