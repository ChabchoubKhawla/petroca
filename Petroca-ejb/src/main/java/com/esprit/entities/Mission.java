package com.esprit.entities;

import java.io.Serializable;
import java.lang.String;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import com.esprit.type.TypeStatusMission;

/**
 * Entity implementation class for Entity: Mission
 *
 */
@Entity

public class Mission implements Serializable {

	   
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String trajet;
	private double cout;
	@Temporal(TemporalType.TIMESTAMP)
	private Date approximative_time;
	private static final long serialVersionUID = 1L;
	@OneToMany(mappedBy="mission")
	private List<Users> user;
	@OneToMany(mappedBy="mission")
	private List<Camions> camion;
	private TypeStatusMission status;

	public TypeStatusMission getStatus() {
		return status;
	}
	public void setStatus(TypeStatusMission status) {
		this.status = status;
	}
	public List<Camions> getCamion() {
		return camion;
	}
	public void setCamion(List<Camions> camion) {
		this.camion = camion;
	}
	public List<Users> getUser() {
		return user;
	}
	public void setUser(List<Users> user) {
		this.user = user;
	}
	public Mission() {
		super();
	}   
	  
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTrajet() {
		return this.trajet;
	}

	public void setTrajet(String trajet) {
		this.trajet = trajet;
	}   
	public double getCout() {
		return this.cout;
	}

	public void setCout(double cout) {
		this.cout = cout;
	}   
	public Date getApproximative_time() {
		return this.approximative_time;
	}

	public void setApproximative_time(Date approximative_time) {
		this.approximative_time = approximative_time;
	}
   
}
