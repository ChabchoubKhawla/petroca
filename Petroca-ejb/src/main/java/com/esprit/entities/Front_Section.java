package com.esprit.entities;

import java.io.Serializable;
import java.lang.String;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Front_Section
 *
 */
@Entity

public class Front_Section implements Serializable {

	   
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String data;
	private Date date;
	@OneToMany(mappedBy="front_section")
	private List <Users> user;
	private static final long serialVersionUID = 1L;

	public Front_Section() {
		super();
	}   
	
	

	public List<Users> getUser() {
		return user;
	}



	public void setUser(List<Users> user) {
		this.user = user;
	}



	  
	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getData() {
		return this.data;
	}

	public void setData(String data) {
		this.data = data;
	}   
	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
   
}
