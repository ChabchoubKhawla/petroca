package com.esprit.type;

public enum TypeOil {
	UNLEADED_GASOLINE_95, 
	UNLEADED_GASOLINE_98,
	NATURAL_GAZ,
	GASOIL50,
	GASOIL,
}
