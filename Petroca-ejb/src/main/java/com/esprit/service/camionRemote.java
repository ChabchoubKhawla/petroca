package com.esprit.service;

import java.util.ArrayList;

import javax.ejb.Remote;

import com.esprit.entities.Camions;

@Remote
public interface camionRemote {
	public void addCamion(int id);
	public void deleteCamion(int id);
	public ArrayList<Camions> readAllCamion();
	public Camions searchCamion(String matricule);
}
