package com.esprit.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

import com.esprit.entities.EtatReclamation;
import com.esprit.entities.Reclamation;
import com.esprit.entities.Users;
import com.esprit.type.TypeReclamation;

/**
 * Session Bean implementation class ReclamationServices
 */
@Stateless
@LocalBean
public class ReclamationServices implements ReclamationServicesRemote, ReclamationServicesLocal {

	@PersistenceContext(unitName = "Petroca-ejb")
	EntityManager em;

	public ReclamationServices() {

	}

	@Override
	public int addReclamation(Reclamation R) {
		// TODO Auto-generated method stub

		em.persist(R);
		return R.getId();
	}

	@Override
	public void affectUserReclamation(int userID, int RecId) {
		// TODO Auto-generated method stub

		Users user = em.find(Users.class, userID);
		Reclamation reclamation = em.find(Reclamation.class, RecId);
		user.getReclamation().add(reclamation);
		reclamation.setUsers(user);

	}

	@Override
	public void removeReclamationByID(int id) {
		// TODO Auto-generated method stub

		em.remove(em.find(Reclamation.class, id));

	}

	@Override
	public List findByType(TypeReclamation type) {
		// TODO Auto-generated method stub

		return em.createQuery("SELECT c FROM Reclamation c WHERE c.type = :Rtype").setParameter("Rtype", type)
				.getResultList();
	}

	@Override
	public void updateDescription(Reclamation R, String description) {
		// TODO Auto-generated method stub
		if (em.find(Reclamation.class, R.getId()) != null && (R.getEtat().equals(EtatReclamation.En_attente))) {
			Reclamation reclamation = em.find(Reclamation.class, R.getId());
			reclamation.setDescription(description);
		}
	}

	@Override
	public Reclamation searchById(int id) {
		// TODO Auto-generated method stub
		return em.find(Reclamation.class, id);
	}

	@Override
	public List<Reclamation> findAll() {
		// TODO Auto-generated method stub
		System.out.println("ok find all");
		ArrayList<Reclamation> reclamations = (ArrayList<Reclamation>) em.createQuery("Select c from Reclamation c")
				.getResultList();
		return reclamations;

	}

}
