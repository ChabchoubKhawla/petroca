package com.esprit.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remote;

import com.esprit.entities.Users;
import com.esprit.type.TypeUser;

@Remote
public interface UserServicesRemote {

	
	public int addUser(Users user);
	public void removeUser(int id);
	public void Update(Users user);
	public Users findById(int id);
	public ArrayList<Users> getAll();
	public List<Users> getAllByType(TypeUser userType);
	public Users getSingleByType(TypeUser userType, int id);
	
}
