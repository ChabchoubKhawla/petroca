package com.esprit.service;

import java.util.List;

import javax.ejb.Remote;

import com.esprit.entities.Forage;
import com.esprit.entities.Users;

@Remote
public interface AuthenticationServiceRemote {

	
	public boolean checkEmail(String email);
	public boolean checkPassword(String password);
	public boolean checkDatabase(String email, String password);
	
	public Users authenticate(String email, String password);
	public void authenticateClient(String username, String password);
	
	
	public Forage getAll();
}
