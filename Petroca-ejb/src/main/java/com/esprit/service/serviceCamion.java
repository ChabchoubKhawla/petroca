package com.esprit.service;

import java.util.ArrayList;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.esprit.entities.Camions;

@Stateless
public class serviceCamion implements camionRemote{
	
	@PersistenceContext
	EntityManager em;
	
	@Override
	public void addCamion(int id) {
		em.persist(id);
		
	}

	@Override
	public void deleteCamion(int id) {
		
		if(em.find(Camions.class,id)!=null){
			Camions c=em.find(Camions.class,id);	
			em.remove(c);
		}
		else{
			System.out.println("Camion not exist");
		}
	}

	@Override
	public ArrayList<Camions> readAllCamion() {
		TypedQuery<Camions> query=em.createQuery("select c from camions",Camions.class);
		ArrayList<Camions> results=(ArrayList<Camions>) query.getResultList();
		return results;
	}

	@Override
	public Camions searchCamion(String matricule) {
		TypedQuery<Camions> query=em.createQuery("select c from camions where ",Camions.class);
		return null;
	}

}
