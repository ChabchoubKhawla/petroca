package com.esprit.service;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.esprit.entities.Forage;
import com.esprit.entities.Users;

/**
 * Session Bean implementation class AuthenticationService
 */
@Stateless
@LocalBean
public class AuthenticationService implements AuthenticationServiceRemote, AuthenticationServiceLocal {

	
	@PersistenceContext
	EntityManager entityManager;
	private List<Users> users;
	
    /**
     * Default constructor. 
     */
    public AuthenticationService() {
        // TODO Auto-generated constructor stub
    }

   


	

	@Override
	public boolean checkEmail(String email) {
		String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
	}

	@Override
	public boolean checkPassword(String password) {
		if(!password.equals(""))
				return true;
		return false;
	}

	@Override
	public boolean checkDatabase(String email, String password) {
			Query query = entityManager.createQuery("SELECT u from Users u WHERE u.email = :email",Users.class).setParameter("email", email);
			Users user = (Users) query.getSingleResult();
			if(email == user.getEmail() && checkPassword(password)){
				return true;
			}
		return false;
	}

	

	@Override
	public void authenticateClient(String username, String password) {
		Query query =  entityManager.createQuery("SELECT u from Users u WHERE u.username=:username",Users.class).setParameter("username", username);
		Users user = (Users) query.getSingleResult();
		if(username == user.getEmail() && password == user.getPassword()){
			System.out.println("client authenticated");
		}
	}

	@Override
	public Forage getAll() {
		Query query = entityManager.createQuery("SELECT f from Forage f where f.id= :id").setParameter("id", 1);
		
		return (Forage) query.getSingleResult();
	}



	@Override
	public Users authenticate(String email, String password) {
		Users user = (Users) entityManager.createQuery("Select u from Users u WHERE u.email= :email").setParameter("email", email).getSingleResult();
			if(checkEmail(email) && user.getPassword().equals(password))
					return user;
		return null;
	}
	
	
	
	


}
