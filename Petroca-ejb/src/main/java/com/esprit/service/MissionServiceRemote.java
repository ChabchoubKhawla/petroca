package com.esprit.service;

import java.util.List;

import javax.ejb.Remote;

import com.esprit.entities.Mission;
import com.esprit.type.TypeStatusMission;
;

@Remote
public interface MissionServiceRemote {
	
	
	
	public int addMission(Mission mission);
	public void removeMission(int id);
	public void UpdateMission(Mission mission);
	public Mission findById(int id);
	public List<Mission> getAll();
	public List<Mission> getAllByStatus(TypeStatusMission missionStatus);
	public Mission getSingleByStatus(TypeStatusMission missionStatus, int id);
}
