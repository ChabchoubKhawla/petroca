package com.esprit.service;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.esprit.entities.Mission;
import com.esprit.type.TypeStatusMission;

/**
 * Session Bean implementation class MissionService
 */
@Stateless
@LocalBean
public class MissionService implements MissionServiceRemote, MissionServiceLocal {

	@PersistenceContext
	EntityManager entityManager;
	Mission mission;
    /**
     * Default constructor. 
     */
    public MissionService() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public int addMission(Mission mission) {
			entityManager.persist(mission);
		return mission.getId();
	}

	@Override
	public void removeMission(int id) {
		entityManager.remove(mission);
	}

	@Override
	public void UpdateMission(Mission mission) {
		
		entityManager.persist(mission);
		
	}

	@Override
	public Mission findById(int id) {
		mission = entityManager.find(Mission.class, id);
		return null;
	}

	@Override
	public List<Mission> getAll() {
		List<Mission> list = entityManager.createQuery("SELECT m from Mission m").getResultList();
		return list;
	}

	@Override
	public List<Mission> getAllByStatus(TypeStatusMission missionStatus) {
		List<Mission> list = entityManager.createQuery("SELECT m from Mission m where m.status = :status").setParameter("status", missionStatus).getResultList();
		return list;
	}

	@Override
	public Mission getSingleByStatus(TypeStatusMission missionStatus, int id) {
		Mission mission = (Mission) entityManager.createQuery("SELECT m from Mission m where m.status = :status and m.id= :id").setParameter("status", missionStatus).setParameter("id",id).getSingleResult();
		return mission;
	}

}
