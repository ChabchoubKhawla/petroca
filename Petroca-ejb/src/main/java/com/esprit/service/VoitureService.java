package com.esprit.service;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.esprit.entities.Voitures;

/**
 * Session Bean implementation class VoitureService
 */
@Stateless
@LocalBean
public class VoitureService implements VoitureServiceRemote, VoitureServiceLocal {

	
	@PersistenceContext
	EntityManager entityManager;
	
    /**
     * Default constructor. 
     */
    public VoitureService() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public int addVoiture(Voitures v) {
		System.out.println("Adding a new car"+v.getId());
		entityManager.persist(v);
		return v.getId();
	}

	@Override
	public void removeVoiture(int id) {
		System.out.println("Deleting car"+id);
		entityManager.remove(entityManager.find(Voitures.class, id));
		
	}

	@Override
	public void updateVoitures(Voitures v) {
		///do the shit you want to do
		entityManager.persist(v);
		
	}

	@Override
	public Voitures findById(int id) {
		Voitures voiture = entityManager.find(Voitures.class, id);
		return voiture;
	}

	@Override
	public List<Voitures> getAll() {
		List<Voitures> CarList =  entityManager.createQuery("SELECT v from Voitures v").getResultList();
		return CarList;
	}

}
