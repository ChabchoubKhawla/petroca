package com.esprit.service;


import java.util.List;
import javax.ejb.Local;

import com.esprit.entities.Reclamation;
import com.esprit.type.TypeReclamation;


@Local
public interface ReclamationServicesLocal {
	
	public int addReclamation(Reclamation R);//done 
	public void affectUserReclamation(int userID , int RecId);//done
	public void removeReclamationByID(int id);//done 
	public List<Reclamation> findByType(TypeReclamation  type);//done
	public Reclamation searchById(int id);//done
	public List<Reclamation> findAll();//done
	public void updateDescription(Reclamation R,String description);




}
