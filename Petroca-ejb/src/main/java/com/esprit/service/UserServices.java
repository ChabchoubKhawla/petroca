package com.esprit.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.esprit.entities.Users;
import com.esprit.type.TypeUser;

/**
 * Session Bean implementation class UserServices
 */
@Stateless
@LocalBean
public class UserServices implements UserServicesRemote, UserServicesLocal {

	
	@PersistenceContext
	EntityManager entityManager;
	Users user;
    /**
     * Default constructor. 
     */
    public UserServices() {
        // TODO Auto-generated constructor stub
    }
	@Override
	public int addUser(Users user) {
		System.out.println("we are adding user to our database"+ user.toString());
		entityManager.persist(user);
		return user.getId();
		
	}
	@Override
	public void removeUser(int id) {
		System.out.println("we are deleting user from our database"+ user.toString());
		entityManager.remove(entityManager.find(Users.class, id));
		entityManager.persist(user);
		
	}
	@Override
	public void Update(Users user) {
		System.out.println("we are updating user from our database"+ user.toString());
		entityManager.persist(user);
		
		
	}
	@Override
	public Users findById(int id) {
		Users user = entityManager.find(Users.class, id);
		return user;
	}
	@Override
	public ArrayList<Users> getAll() {
		// TODO Auto-generated method stub
		ArrayList<Users> users = (ArrayList<Users>) entityManager.createQuery("Select u from Users u").getResultList();
		return  users;
	}
	@Override
	public List<Users> getAllByType(TypeUser userType) {
		List<Users> users = entityManager.createQuery("Select u from Users u where u.userType= :userType").setParameter("userType", userType).getResultList();
		return users;
	}
	@Override
	public Users getSingleByType(TypeUser userType, int id) {
		Users user = (Users) entityManager.createQuery("Select uÒ from Users u where u.userType= :userType and id= :id").setParameter("userType", userType).setParameter("id", id).getSingleResult();		
		
		return user;
	}

	
    
    

}
