package com.esprit.service;


import java.util.List;
import javax.ejb.Remote;

import com.esprit.entities.Reclamation;
import com.esprit.type.TypeReclamation;



@Remote
public interface ReclamationServicesRemote {

	public int addReclamation(Reclamation R);//done 
	public void affectUserReclamation(int userID , int RecId);//done
	public void removeReclamationByID(int id);//done 
	public List<Reclamation> findByType(TypeReclamation  type);//done
	public Reclamation searchById(int id);//done
	public List<Reclamation> findAll();//done
	public void updateDescription(Reclamation R,String description);//done
}