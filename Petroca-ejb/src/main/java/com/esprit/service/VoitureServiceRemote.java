package com.esprit.service;

import java.util.List;

import javax.ejb.Remote;

import com.esprit.entities.Voitures;

@Remote
public interface VoitureServiceRemote {

	
	public int addVoiture(Voitures v);
	public void removeVoiture(int id);
	public void updateVoitures(Voitures v);
	public Voitures findById(int id);
	public List<Voitures> getAll();
	
}
